﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectoriUniti
{
    class Program
    {
        // tema pt acasa
        static void Main(string[] args)
        {
            int[] v1 = new int[5];
            int[] v2 = new int[6];

            // populeaza, sorteaza si afiseaza primul vector
            PopuleazaVector(ref v1);
            AfiseazaVector(v1);
            SorteazaVector(ref v1);
            AfiseazaVector(v1);

            Console.WriteLine();
            // populeaza, sorteaza si afiseaza al doilea vector
            PopuleazaVector(ref v2);
            AfiseazaVector(v2);
            SorteazaVector(ref v2);
            AfiseazaVector(v2);

            Console.WriteLine();
            int[] m = UnesteVectorii(v1, v2);
            AfiseazaVector(m);

            Console.WriteLine();

            GasesteValoare(m, 26);

            Console.ReadKey();
        }

        static void GasesteValoare(int[] v, int nr)
        {
            int limInf = 0;
            int limSup = v.Length - 1;
            int indexCautat = -1;

            while (limInf <= limSup)
            {
                int mij = (limInf + limSup) / 2;

                if (v[mij] == nr)
                {
                    indexCautat = mij;
                    break;
                }
                else if (v[mij] < nr)
                {
                    limInf = mij + 1;
                }
                else
                {
                    limSup = mij - 1;
                }
            }

            if (indexCautat >= 0)
                Console.WriteLine("L-am gasit la pozitia: " + indexCautat);
            else
                Console.WriteLine("Nu l-am gasit :(");
        }

        private static void PopuleazaVector(ref int[] v)
        {
            // we add here a seed, so we can have different values
            // for some reason 2 Random objects created in the same time, will output the same values
            Random rand = new Random(v.Length);

            for (int i = 0; i < v.Length; i++)
            {
                v[i] = rand.Next(100);
            }
        }

        // bubble sort
        static void SorteazaVector(ref int[] v)
        {
            for (int i = 0; i < v.Length - 1; i++)
            {
                for (int j = 0; j < v.Length - i - 1; j++)
                {
                    if (v[j] > v[j + 1])
                    {
                        int tmp = v[j + 1];
                        v[j + 1] = v[j];
                        v[j] = tmp;
                    }
                }
            }
        }

        static void AfiseazaVector(int[] v)
        {
            for (int i = 0; i < v.Length; i++)
            {
                Console.Write(v[i]);

                if (i < v.Length - 1)
                    Console.Write(", ");
            }

            Console.WriteLine();
        }


        static int[] UnesteVectorii(int[] v1, int[] v2)
        {
            int[] m = new int[0];
            int i = 0, j = 0, k = 0;

            // adaugam elementele in functie de cum sunt mai mari
            while (i < v1.Length && j < v2.Length)
            {
                Array.Resize(ref m, k + 1);

                if (v1[i] < v2[j])
                {
                    m[k] = v1[i];
                    i++;
                }
                else
                {
                    m[k] = v2[j];
                    j++;
                }

                k++;
            }

            // adaugam restul elementelor din v1
            for (int a = i; a < v1.Length; a++)
            {
                Array.Resize(ref m, k + 1);
                m[k] = v1[a];
                k++;
            }

            // adaugam restul elementelor din v2
            for (int a = j; a < v2.Length; a++)
            {
                Array.Resize(ref m, k + 1);
                m[k] = v2[a];
                k++;
            }

            return m;
        }
    }
}